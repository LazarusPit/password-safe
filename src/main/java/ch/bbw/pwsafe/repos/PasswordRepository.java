package ch.bbw.pwsafe.repos;

import ch.bbw.pwsafe.entities.Password;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface PasswordRepository extends JpaRepository<Password, Long> {
    Set<Password> findPwEntriesByCategoryId(long category_id);
    Set<Password> findPwEntriesByUserUsername(String username);
    Optional<Password> findById(Long id);
}
