package ch.bbw.pwsafe.services.crypto;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class AESUtil {

    private Cipher cipher = null;
    public boolean ready = false;
    private final String algorithm = "AES";
    private String key;

    public AESUtil() {
        try {
            cipher = Cipher.getInstance(algorithm);
            ready = true;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
            System.out.println("Cipher failed to initialize. / Einrichtung fehlgeschlagen.");
            System.exit(1);
        }
    }

    protected Key parseKey() {
        this.key = padKeyIfNecessary(this.key);
        return new SecretKeySpec(this.key.getBytes(), this.algorithm);
    }

    public String decrypt(String cipherText, String key) {
        try {
            return new String(resolveText(Cipher.DECRYPT_MODE, Hex.decodeHex(cipherText.toCharArray()), key));
        } catch (DecoderException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        return cipherText;
    }

    public String encrypt(String plainText, String key) {
        byte[] plainTextBytes = plainText.getBytes();
        return Hex.encodeHexString(resolveText(Cipher.ENCRYPT_MODE, plainTextBytes, key));
    }

    private byte[] resolveText(int mode, byte[] inputText, String key) {
        this.key = key;
        return resolveText(mode, inputText);
    }

    protected byte[] resolveText(int mode, byte[] inputText) {
        if (cipher == null) {
            return null;
        }
        Key key = parseKey();
        try {
            cipher.init(mode, key);
            return cipher.doFinal(inputText);
        } catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String padKeyIfNecessary(String key) {
        int size = key.length();
        int remainder = (size % 16);
        if (remainder > 0) {
            key += " ".repeat(16 - remainder);
        }
        return key;
    }
}
