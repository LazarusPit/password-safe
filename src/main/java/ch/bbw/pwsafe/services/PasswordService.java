package ch.bbw.pwsafe.services;

import ch.bbw.pwsafe.entities.Password;
import ch.bbw.pwsafe.repos.PasswordRepository;
import ch.bbw.pwsafe.services.crypto.AESUtil;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PasswordService {

    private final String key = "123321";

    private final PasswordRepository repository;
    private final AESUtil crypto;

    public PasswordService(PasswordRepository repository) {
        this.repository = repository;
        this.crypto = new AESUtil();
    }

    public Set<Password> findEntriesByCategoryId(long category_id) {
        return repository.findPwEntriesByCategoryId(category_id);
    }
    public Set<Password> findPwEntriesByUsername(String username) {
        return repository.findPwEntriesByUserUsername(username);
    }
    public Password add(Password password) {
        try {
            return repository.saveAndFlush(password);
        } catch (Exception e) {
         return null;
        }
    }
    public Boolean delete(Long id) {
        try {
            repository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public Password editOrSave(Long id, Password password) {
        String plainText = password.getValue();
        String cipherText = crypto.encrypt(plainText, key);
        password.setValue(cipherText);
        return repository.saveAndFlush(password);
    }
    public Password findById(Long id) {
        Password password = repository.findById(id).orElse(null);
        if (password != null) {
            password.setValue(crypto.decrypt(password.getValue(), key));
        }
        return password;
    }
}
