package ch.bbw.pwsafe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PwSafeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PwSafeApplication.class, args);
    }

}
