package ch.bbw.pwsafe.controllers;

import ch.bbw.pwsafe.entities.Category;
import ch.bbw.pwsafe.entities.Password;
import ch.bbw.pwsafe.entities.User;
import ch.bbw.pwsafe.services.PasswordService;
import ch.bbw.pwsafe.services.UserDetailsService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Set;

@Controller
@RequestMapping("/open-safe")
public class PasswordController {

    private final PasswordService service;
    private final UserDetailsService userService;

    public PasswordController(PasswordService service, UserDetailsService userService) {
        this.service = service;
        this.userService = userService;
    }

    @GetMapping("/")
    public String view(Principal principal, Model model) {
        Set<Password> entries = service.findPwEntriesByUsername(principal.getName());
        model.addAttribute("passwords", entries);
        return "open-safe";
    }

    @PostMapping("/")
    @ResponseBody
    public Password addEntry(@Valid @RequestBody Password password) {
        return this.service.add(password);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public Boolean deleteEntry(@Valid @PathVariable Long id) {
        return this.service.delete(id);
    }

    @GetMapping("/edit/{id}")
    public String editEntry(@Valid @PathVariable Long id, Model model, Principal principal) {
        Password entry = null;
        Set<Category> categories = userService.getByUsername(principal.getName()).getCategories();
        model.addAttribute("categories", categories);
        if (id > 0) {
            entry = service.findById(id);
        }
        if (entry == null) {
            entry = new Password();
        }
        model.addAttribute("entry", entry);
        return "edit";
    }

    @PostMapping(path = "/edit/{id}", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String editOrSave(@Valid @ModelAttribute("entry") Password entry, @PathVariable Long id, Principal principal) {
        User user = userService.getByUsername(principal.getName());
        entry.setUser(user);
        service.editOrSave(id, entry);
        return "redirect:/open-safe/";
    }
}
