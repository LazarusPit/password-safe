package ch.bbw.pwsafe.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Password> getPasswords() {
        return passwords;
    }

    @Column
    private String name;

    @OneToMany
    private Set<Password> passwords;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
