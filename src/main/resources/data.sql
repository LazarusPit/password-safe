INSERT INTO users (enabled, password, username) VALUES (true, '$2a$11$PcfeCVg1.4qj2iwBabDsFeie.1fQo2E/CMHlaZ294a5xMgYagL0d6', 'Admin');
INSERT INTO authorities (authority, username) VALUES ('ROLE_ADMIN', 'Admin'), ('ROLE_USER', 'Admin');
INSERT INTO category (name) VALUES ('Unbestimmt');
INSERT INTO category (name) VALUES ('Privat');
INSERT INTO category (name) VALUES ('Geschäft');
INSERT INTO category (name) VALUES ('Games');
INSERT INTO category (name) VALUES ('Hobbies');
INSERT INTO USERS_CATEGORIES (user_id, categories_id) VALUES(1, 1);
INSERT INTO USERS_CATEGORIES (user_id, categories_id) VALUES(1, 2);
INSERT INTO USERS_CATEGORIES (user_id, categories_id) VALUES(1, 3);
INSERT INTO USERS_CATEGORIES (user_id, categories_id) VALUES(1, 4);
INSERT INTO USERS_CATEGORIES (user_id, categories_id) VALUES(1, 5);

