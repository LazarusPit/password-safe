# Dokumentation
### *Von Benjamin Kelly und David Lévy*

Von Begin an zerbrachen wir unsere Köpfe über das Spring Boot Security Konzept.
Es ist eine Weile her, seit wir uns mit Spring befassten.

Unser ursprüngliches Gedankenbild davon war den gewöhnlichen Login-Ablauf abzubilden, im Grunde genommen: Anmeldedaten annehmen, überprüfen und bei übereinstimmung Tokens zuzuweisen.

Die erste Hürde, über welche wir stolperten, entstand im ersten Schritt. Wir plagten uns über einer der allzu bekannten Informationsfluten der Java-Exceptions, was sich am Ende als eine fehlende DB-Tabelle (Authority) herausgab. 

Als Nächstes kam es zu Streitigkeiten zwischen uns über das entsprechende Verschlüsselungsverfahren der Passwörter-Einträge. Wir konnten uns nicht entscheiden, was ideal sein wäre. Einerseits befürwortete man für den Schlüssel eine simple Kombination von Timestamp und Benutzername, anderseits wurde das Passwort des Benutzers (pre-hash) vorgeschlagen.

Die hervorragendste Idee (im bescheidensten Sinne), die uns eingefallen ist, war eine Implementation von RSA. 

Schluss am Ende lieferten wir eine AES Verschlüsselung durch den hardgecodeten Schlüssel lautend "123321".