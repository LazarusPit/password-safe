# Grundkonzept - Password Safe
### *Von Benjamin Kelly und David Lévy*
___

### Interne Sicherheit:

> Mit symetrischer Verschlüsselung umzusetzen

### Externe Kommunikation:

> Über asymetrische Verschlüsselung gesichert
>
> HTTPS (TLS) wenn möglich

### Mockup DB Technologie: 

> h2

### Sicherheitslücken zu vermeiden

> - XSS \- `AntiSamy`
> - CSRF \- `Spring Boot Security`
> - Broken Authentication - `Spring Session + Login Logik`

### Aufbau:

> Es soll nach MVC-Prinzip geschehen.


### Achtung! 

>*PasswortSafes in der Form von Webapplikationen sind sicherheitstechnische Alpträume.*